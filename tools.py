import os,struct,numpy
from OpenGL.GL import *
from cls_lib import buffer,vBuffer,setPrimitivesObj
from menu_res import winerror

class texture:

    def __init__(self,data,mode,size_x,size_y): # mode rgb/ rgba
        self.texBuffer = glGenTextures(1)
        glBindTexture(GL_TEXTURE_2D, self.texBuffer)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)

        glTexImage2D(GL_TEXTURE_2D, 0, mode, size_x, size_y, 0, mode, GL_UNSIGNED_BYTE, data)
        glGenerateMipmap(GL_TEXTURE_2D)

    def getID(self):
        return self.texBuffer

def getLine():
    vao = vBuffer()
    vbo = buffer(GL_ARRAY_BUFFER, numpy.array([(0,0,0),(0,0.2,0)],dtype="float32"), GL_STATIC_DRAW)
    ebo = buffer(GL_ELEMENT_ARRAY_BUFFER, numpy.array([0,1],dtype='uint'), GL_STATIC_DRAW)
    vao.newAttrib(3, GL_FLOAT, GL_TRUE, 12)
    f = setPrimitivesObj(GL_LINES,None,vao,ebo,vbo)
    f.useModel_off()
    return f

class configuration:

    def __init__(self,atribs=[]):
        self.obj_fld = None
        self.shdr_fld = None

        self.get_atrb = atribs
        self.collector = {}

    def parse_cfg(self,path):
        try:
            file = open(path,"r")
        except:
            winerror("missing config file!")
        data = file.readlines()
        for i in range(len(data)):
            data[i] = data[i].strip().split("=")
            for l in self.get_atrb:
                if (data[i][0] == l):
                    self.collector[l] = data[i][1]
                    break
        file.close()

    def parse_all(self,path):
        try:
            file = open(path, "r")
        except:
            winerror("missing config file")
        data = file.readlines()
        for i in range(len(data)):
            data[i] = data[i].strip().split("=")
            self.collector[data[i][0]] = data[i][1]
        file.close()

def customModel(dr_type,vao,ebo,vbo):
    s = object()
    s.dr = dr_type
    s.vao = vao
    s.ebo = ebo
    s.vbo = vbo
    return s

class object:

    def __init__(self):
        self.dr = None
        self.vert = []
        self.ind = []
        self.name = None

        self.ind_amount = 0

        self.ebo = None
        self.vao = None
        self.vbo = None

    def drawing_selector(self,byte):
        if (byte == 0):
            self.dr = GL_TRIANGLES
        elif (byte == 1):
            self.dr = GL_QUADS
        elif (byte == 2):
            self.dr = GL_POINT
        elif (byte == 3):
            self.dr = GL_LINES

    def delete(self):
        del self

    def create_vertices(self,data):
        for i in range(0,len(data),8):
            self.vert.append(struct.unpack("d",data[i:i+8])[0])
        self.vert = numpy.array(self.vert,dtype="float32")

    def create_indices(self,data):
        for i in range(0,len(data),4):
            self.ind.append(int.from_bytes(data[i:i+4],"little"))
        self.ind = numpy.array(self.ind,dtype="uint")
        self.ind_amount = self.ind.size

    def set_buffers(self):
        self.vao = vBuffer()
        self.vbo = buffer(GL_ARRAY_BUFFER,self.vert,GL_STATIC_DRAW)
        self.ebo = buffer(GL_ELEMENT_ARRAY_BUFFER,self.ind,GL_STATIC_DRAW)
        self.vao.newAttrib(3,GL_FLOAT,GL_TRUE,12,0)

    def getPrimObject(self):
        s = setPrimitivesObj(self.dr,None,self.vao,self.ebo,self.vbo)
        s.useModel_off()
        return s

def create_vertex_fld(fld):
    try:
        os.chdir(fld)
        os.chdir("../")
    except:
        os.mkdir(fld)

def load_file(fld,name):
    c_ofst = 0
    file_ = open(f"{fld}\\{name}","rb")
    data = bytearray(file_.read())
    obj = object()
    obj.drawing_selector(data[c_ofst])
    c_ofst =+ 1
    amount = int.from_bytes(data[c_ofst:c_ofst+4], "little")
    c_ofst += 4
    obj.create_vertices(data[c_ofst:c_ofst+amount])
    c_ofst += amount
    amount = int.from_bytes(data[c_ofst:c_ofst+4], "little")
    c_ofst += 4
    obj.create_indices(data[c_ofst:c_ofst+amount])
    obj.name = name
    return obj


#load_file("objects","test_3_g")
