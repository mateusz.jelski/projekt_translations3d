from OpenGL.GL import *
import pygame
import numpy
import ctypes,glm

class HW:

    def __init__(self,x,y,flg):
        self.x = x
        self.y = y
        self.win = pygame.display.set_mode((x,y),flg)

    def hEvents(self):
        for i in pygame.event.get():
            if (i.type == True):
                return True
class buffer:

    def __init__(self,buffer_type,data,usage):
        self.bf = glGenBuffers(1)
        self.type = buffer_type

        glBindBuffer(self.type,self.bf)
        glBufferData(self.type,data,usage)

    def bind(self):
        glBindBuffer(self.type,self.bf)

def setObjectAt(pos,obj,custom_model=numpy.array(glm.mat4(1.0)),use_model=False):
    if (use_model == True):
        obj.model = numpy.array(glm.translate(custom_model,pos))
    else:
        obj.model = numpy.array(glm.translate(obj.model, pos))

class setPrimitivesObj:

    def __init__(self,draw_type,modelLocation,vbo=None,ebo=None,vao=None):
        self.indices = ebo
        self.vertex_attrib = vbo
        self.vertex = vao
        self.modLoc = modelLocation
        self.amount = 0

        self.type = draw_type
        self.model = numpy.array((glm.mat4(1.0)))
        self.bindModel = True

    def draw(self,quantity,ofst=0,texBind=False):
        self.indices.bind()
        self.vertex_attrib.bind()
        if (self.bindModel == True):
            glUniformMatrix4fv(self.modLoc,1,GL_FALSE,self.model)
        if (texBind != False):
            glBindTexture(GL_TEXTURE_2D,texBind)
        glDrawElements(self.type,quantity,GL_UNSIGNED_INT,ctypes.c_void_p(ofst))
        glBindVertexArray(0)
        if (texBind != False):
            glBindTexture(GL_TEXTURE_2D,0)

    def setModel(self,model):
        self.model = model

    def useModel_on(self):
        self.bindModel = True

    def useModel_off(self):
        self.bindModel = False

    def getModel(self):
        return self.model

class vBuffer:

    def __init__(self):
        self.bf = glGenVertexArrays(1)
        self.cIndex = 0
        glBindVertexArray(self.bf)

    def newAttrib(self,size,dtype,normalize,stride,ofst=0):
        glEnableVertexAttribArray(self.cIndex)
        glVertexAttribPointer(self.cIndex,size,dtype,normalize,stride,ctypes.c_void_p(ofst))
        self.cIndex += 1

    def getIndex(self):
        return self.cIndex

    def bind(self):
        glBindVertexArray(self.bf)

class shader_code:

    def __init__(self,src,shader_type):
        #self.type = shader_type
        #self.src_code = src
        self.shader = glCreateShader(shader_type) # GL_VERTEX_SHADER, GL_FRAGMENT_SHADER
        glShaderSource(self.shader,src)
        glCompileShader(self.shader)

    def destroy(self):
        glDeleteShader(self.shader)

    def getShader(self):
        return self.shader

class shader_program:

    def __init__(self,shrds,DEL = True):
        self.program = glCreateProgram()
        self.uniTypes = ["vec3f","vec3i","mat4f","vec4f","vec4i"]
        self.uniCmd = [glUniform3f,glUniform3i,glUniformMatrix4fv,glUniform4f,glUniform4i]
        self.uniforms = {}
        for i in shrds:
            glAttachShader(self.program,i.shader)
        glLinkProgram(self.program)

        if (DEL == True):
            for i in shrds:
                i.destroy()

    def run(self):
        glUseProgram(self.program)

    def addUniform(self,name):
        self.uniforms[name] = glGetUniformLocation(self.program,name)

    def setUniform(self,name,val,dtype):
        for i in range(len(self.uniTypes)):
            if (dtype == self.uniTypes[i]):
                try:
                    self.uniCmd[i](name,*val)
                    return 1
                except Exception as exce:
                    print(exce)
                    raise ValueError("bad arguments or values")
        raise ValueError("Unsupported type")

def mat():
    return numpy.array((glm.mat4(1.0)))

def intersect(a, b):
  return ((a.minX <= b.maxX and a.maxX >= b.minX) and (a.minY <= b.maxY and a.maxY >= b.minY) and (a.minZ <= b.maxZ and a.maxZ >= b.minZ))

class collider_box:

    def __init__(self,pos,size):
        self.maxX = pos[0] + size[0]
        self.minX = pos[0]
        self.maxY = pos[1] + size[1]
        self.minY = pos[1]
        self.maxZ = pos[2] + size[2]
        self.minZ = pos[2]