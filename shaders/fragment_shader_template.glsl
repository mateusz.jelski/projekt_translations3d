#version 330
out vec4 FragColor;

uniform vec4 colorSelector;
void main()
{
    if (colorSelector.w == 1.0)
    {
        FragColor = colorSelector;
    }
    else
    {
        FragColor = vec4(0.0,0.5,0.2,1.0);
    }
}
