#version 330
out vec4 FragColor;
in vec2 texCoord_out;

uniform sampler2D SAMPLE;
uniform vec4 colorSelector;
void main()
{
    if (colorSelector.w == 1.0)
    {
        FragColor = colorSelector;
    }
    else
    {
        FragColor = texture(SAMPLE, texCoord_out);
    }
}
