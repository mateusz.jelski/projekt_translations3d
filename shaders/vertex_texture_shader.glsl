#version 330
layout (location = 0) in vec3 pos;
layout (location = 1) in vec2 texCoord;

uniform mat4 model;
uniform mat4 projection;
uniform mat4 view;
out vec2 texCoord_out;
void main() {
    gl_Position = view * projection * model * vec4(pos,1.0);
    texCoord_out = texCoord;

}
