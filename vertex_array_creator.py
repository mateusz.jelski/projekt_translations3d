from tools import *
from OpenGL.GL import GL_TRIANGLES,GL_LINES,GL_QUADS,GL_POINT
import struct

c_object = None
logs = []
class file_obj:

    def __init__(self):
        self.vertices = []
        self.vertex_attrib_data = []
        self.indices = []

        self.atribs = []

        self.primitives = GL_TRIANGLES
        self.saveTofld = "objects"

    def pack(self,filename):
        file_content = b"" + conv_gl_dr_type(self.primitives)
        file_ = open(f"{self.saveTofld}\\{filename}","wb")
        file_content += (len(self.vertices) * 24).to_bytes(4,"little")

        for i in self.vertices:
            for vert in i:
                file_content += struct.pack("d",vert)
        file_content += (len(self.indices) * 4).to_bytes(4,"little")

        for l in self.indices:
            file_content += l.to_bytes(4,"little")
        file_.write(file_content)
        file_.close()

def conv_gl_dr_type(a):
    if (a == GL_TRIANGLES):
        return b"\x00"
    if (a == GL_QUADS):
        return b"\x01"
    if (a == GL_POINT):
        return b"\x02"
    if (a == GL_LINES):
        return b"\x03"

def obj_isLoaded():
    if (c_object == None):
        return False
    return True

def newlog(string):
    global logs
    logs.append(string)

def new_obj():
    global c_object
    c_object = file_obj()
    newlog("Created!")

def print_logs(log_var):
    for i in range(len(log_var)):
        print(log_var[i])
    return []

def toFloat(array):
    for i in range(len(array)):
        array[i] = float(array[i])
    return array

def sizeof(list_):
    size = 0
    for i in list_:
        if (type(i) == int):
            size += 4
        if (type(i) == float):
            size += 8
        if (type(i) == str):
            size += len(i)
    return size

def define_vertex(input_usr):
    if (obj_isLoaded() == False):
        newlog("Object file is not loaded. Load object file or create new.")
        return 0
    pos = toFloat(input_usr[1:])
    if (len(pos) != 3):
        newlog("Incorrect vertex position")
        return 0
    c_object.vertices.append(pos)
    logs.append(f"Added new vertex: {pos}")

def drawingType(input_usr):
    global c_object
    if (obj_isLoaded() == False):
        newlog("Object file is not loaded. Load object file or create new.")
        return 0
    if (len(input_usr) != 2):
        newlog("Incorrect drawing mode")
        return 0
    types = {"GL_TRIANGLES":GL_TRIANGLES,"GL_LINES":GL_LINES,"GL_QUADS":GL_QUADS,"GL_POINT":GL_POINT}
    try:
        dr_type = types[input_usr[1]]
        c_object.primitives = dr_type
        newlog(f"Set drawing type to: {input_usr[1]}")
    except:
        newlog(f"Incorrect drawing type: {input_usr[1]}")

def addIndices(input_usr):
    global c_object
    if (obj_isLoaded() == False):
        newlog("Object file is not loaded. Load object file or create new.")
        return 0
    ind = input_usr[1:]
    for i in range(len(ind)):
        if (int(ind[i]) > len(c_object.vertices)):
            newlog("Vertex does not exist.")
            return 0
        else:
            ind[i] = int(ind[i])
    for i in ind:
        c_object.indices.append(i)

def showVertex():
    if (obj_isLoaded() == False):
        newlog("Object file is not loaded. Load object file or create new.")
        return 0
    for i in c_object.vertices:
        print(i)

def showInd():
    if (obj_isLoaded() == False):
        newlog("Object file is not loaded. Load object file or create new.")
        return 0
    for i in c_object.indices:
        print(i)

def pack(input_usr,fld):
    if (obj_isLoaded() == False):
        newlog("Object file is not loaded. Load object file or create new.")
        return 0
    if (len(input_usr) != 2):
        newlog("incorrect object name")
        return 0
    c_object.saveTofld = fld
    c_object.pack(str(input_usr[1]))

cfg = configuration(["obj_path"])
cfg.parse_cfg("config")
cmd_short = [".new","vertex","draw_mode","indices","show_vertex","show_indice","pack"]
cmd = [new_obj,lambda: define_vertex(input_),lambda: drawingType(input_),lambda: addIndices(input_),showVertex,showInd,lambda: pack(input_,cfg.collector["obj_path"])]

create_vertex_fld(cfg.collector["obj_path"])
while True:
    input_ = str(input(": ")).split(" ")
    for i in range(len(cmd_short)):
        if (input_[0] == cmd_short[i]):
            try:
                cmd[i]()
            except:
                print("Unknow error!")

    logs = print_logs(logs)



