from cls_lib import *
from tools import *
import pygame,threading
from pygame.locals import *
import glm, numpy, os, math
from OpenGL.GL import *
from menu_res import *
import importlib
from exces import Thread_stop

class TransformationExecutor:
    def __init__(self):
        self.cmd = [glm.scale, glm.rotate, glm.translate]

    def execute(self, transformations_, objects_,):
        mat4_ = numpy.array(glm.mat4(1.0))
        #---------------------------------------------------------------------------------
        # lista transformacji moze byc zrobiona naprzemian dla kilku obiektow
        # w chwili obecnej zalezy mi na wydobyciu id unikatowych obiektow
        unique_objects=list(map(lambda transformations_: transformations_.object,transformations_ ))
        unique_objects=numpy.unique(unique_objects).tolist()
        #---------------------------------------------------------------------------------
        # iteracja po ID obiektow znajdujacej sie w liscie przeksztalcen
        for obj in unique_objects:
            obj_matrix=mat4_#numpy.eye(4)  #<- tworzenie macierzy poczatkowej
            # iteracja po samych transformacjach
            for tran_ in transformations_:
                # jezeli transformacja dotyczy naszego obiektu
                if (tran_.object==obj):
                    if (tran_.use == True):
                        if tran_.type=="translate":
                            obj_matrix=numpy.array(self.cmd[2](obj_matrix, tran_.args))
                        elif (tran_.type == "scale"):
                            obj_matrix=numpy.array(self.cmd[0](obj_matrix,tran_.args))
                        elif (tran_.type == "rotate"):
                            obj_matrix=numpy.array(self.cmd[1](obj_matrix,math.radians(tran_.args[0]),tran_.args[1:]))
            # zakonczono wymnazanie macierzy dla danego obiektu
            objects_[obj].setModel(obj_matrix)

def mat44():
    return numpy.array(glm.mat4(1.0))

def main_menu(core,CON):

    root = Tk()
    #root.geometry("500x500")
    addObjectEntry = Entry(root)
    addButton = Button(root,text='add object',command=lambda: addNewDrawing(mat44(),addObjectEntry.get(),core))
    scaleWinButton = Button(root,text="Scale",command=lambda: SCALE(core))
    rotateWinButton = Button(root, text="Rotate", command=lambda: ROTATE(core))
    translateWinButton = Button(root, text="Translate", command=lambda: TRANSLATE(core))
    obj = Button(root, text="All objects", command=lambda: seekObjects(core.to_draw_objects))
    cmds = Button(root, text="All commands", command=lambda: seekCMD(core.command))
    #pop = Button(root, text="Pop Model", command=lambda: POP(core))
    sc_guid = Label(root,text="Load Script")
    sc_entry = Entry(root)
    sc_load = Button(root,text="Load",command=lambda: sctl_add(sc_entry.get(),core))

    delL = Label(root,text="Delete index")
    delEntry = Entry(root)
    butDelCommand = Button(root,text="delete command",command=lambda: core.delCommand(delEntry.get()))
    butDelObject = Button(root, text="delete object",command=lambda:core.delObject(delEntry.get()))
    centerRun = Button(root,text="center",command=lambda: operation_center(core))

    addObjectEntry.grid(column=0,row=0)
    addButton.grid(column=1,row=0)
    scaleWinButton.grid(row=1,column=0)
    rotateWinButton.grid(row=1,column=1)
    translateWinButton.grid(row=1,column=2)
    #pop.pack()
    obj.grid(row=2,column=0)
    cmds.grid(row=2,column=1)
    sc_guid.grid(row=3,column=0)
    sc_entry.grid(row=3,column=1)
    sc_load.grid(row=3,column=2)

    delL.grid(row=4,column=0)
    delEntry.grid(row=4,column=1)
    butDelCommand.grid(row=4,column=2)
    butDelObject.grid(row=4,column=3)
    centerRun.grid(row=5,column=0)

    root.mainloop()
    core.menu = False
    CON.raiseException()

class connector:

    def __init__(self,thr):
        self.thr = thr

    def kill(self):
        self.thr.join(2)

    def raiseException(self):
       raise Thread_stop("End Task")

    def start(self):
        self.thr.start()

class main_core:

    def __init__(self):
        pygame.init()
        self.shdr_v = None
        self.shdr_f = None
        self.program = None
        self.sc_fld = None
        self.ctr = 0

        self.objects = []
        self.shaders = []
        self.runcode = True

        self.error_logs = []
        self.win = None

        self.to_draw_objects = []
        self.command = []
        self.command_executer = TransformationExecutor()  #executer()
        self.bg_color = ()
        self.menu = False
        self.grid_draw = False

        self.sc_loaded = []
        self.sctl = []
        self.std_move = (0,0,-1)

    def delObject(self,index):
        try:
            index = int(index)
        except: return 0
        try:
            a = self.to_draw_objects[index]
        except:
            self.error_logs.append("Cannot delete object: Object does not exist")
            return 0
        del self.to_draw_objects[index]
        i = 0
        while i < len(self.command):
            if (self.command[i].object == index):
                del self.command[i]
            else:
                i += 1

    def showGridEnable(self,enable):
        self.grid_draw = enable

    def delCommand(self,index):
        try:
            index = int(index)
        except: return 0
        try:
            a = self.command[index]
            del self.command[index]
        except:
            self.error_logs.append("Cannot delete command: Object does not exist")
            return 0

    def enableFunctions(self,blend,depth):
        glActiveTexture(GL_TEXTURE0)
        if (blend == True):
            glEnable(GL_BLEND)
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
        if (depth == True):
            glEnable(GL_DEPTH_TEST)

    def createTexture(self,bytedata,mode,x,y):
        s = texture(bytedata,mode,x,y)
        glBindTexture(GL_TEXTURE_2D,0)
        return s.getID()

    def bindTextureTo(self,tex,index):
        self.to_draw_objects[index].texture = tex

    def drawGrid(self,mod):
        if (self.grid_draw == True):
            halfpi = math.pi/2
            mod = mod
            color = [(0,0,1,1),(1,0,0,1),(0,1,0,1)]
            translate = [(0,1,0),(1,0,0),(0,0,1)]
            #glUniformMatrix4fv(self.program.uniforms["model"], 1, GL_FALSE, mod)
            for i in range(3):
                mod = numpy.array(glm.rotate(mod,halfpi,translate[i]))
                glUniformMatrix4fv(self.program.uniforms["model"],1,GL_FALSE,mod)
                glUniform4f(self.program.uniforms["colorSelector"], color[i][0],color[i][1],color[i][2],color[i][3])
                self.objects["__grid_line_"].draw(2)
            glUniform4f(self.program.uniforms["colorSelector"],0,0,0,0)

    def ioevent(self):
        if (pygame.key.get_pressed()[pygame.K_ESCAPE] == True):
            self.runMenu()
        for i in self.sctl:
            self.runScript(i)
        if (self.sctl != 0):
            self.sctl = []

    def runScript(self,sc_name):
        try:
            self.sc_loaded.append(importlib.import_module(f"{self.sc_fld}.{sc_name}").script())
            try:
                self.sc_loaded[-1].setup(self)
            except Exception as exc:
                self.error_logs.append("Cannot exec setup: " + sc_name + str(exc))
                del self.sc_loaded[-1]
        except Exception as exc:
            self.error_logs.append("Cannot load script: " + sc_name +" "+ str(exc))
        #self.sc_loaded.append(importlib.import_module(f"{self.sc_fld}.jenny_demo").script())
        #self.sc_loaded[-1].setup(self)

    def getObjectID(self):
        return len(self.to_draw_objects) - 1

    def runMenu(self):
        if (self.menu == False):
            self.menu = connector(None)
            self.menu.thr = threading.Thread(target=lambda: main_menu(CORE, self.menu))
            self.menu.start()

    def getObjectByIndex(self,index):
        return self.to_draw_objects[index]

    def getCommand(self,index):
        return self.command[index]

    def objectCreate(self,name):
        addNewDrawing(mat44(),name,self)
        return self.getObjectID()

    def InstructionCreate(self,index,type,args):
        newInstruction(index,args,type,self)

    def load_objects_names(self,fld):
        os.chdir(fld)
        self.objects = os.listdir()
        bf = {}
        os.chdir("../")
        for i in range(len(self.objects)):
            bf[self.objects[i]] = load_file(fld,self.objects[i])
            del_ptr = bf[self.objects[i]]
            am = bf[self.objects[i]].ind_amount
            bf[self.objects[i]].set_buffers()
            bf[self.objects[i]] = bf[self.objects[i]].getPrimObject()
            bf[self.objects[i]].amount = am
            del_ptr.delete()
        self.objects = bf
        self.objects["__grid_line_"] = getLine()

    def winCreate(self,x,y):
        self.win = HW(x,y,OPENGL|HWSURFACE|DOUBLEBUF)
        self.error_logs.append(f"Window created: {x},{y}")

    def customModelApply(self,draw_type,vao,ebo,vbo,name,ind_amount):
        csm = customModel(draw_type,vao,ebo,vbo)
        self.objects[name] = csm.getPrimObject()
        self.objects[name].amount = ind_amount

    def runProgram(self):
        self.program.run()

    def useScLoop(self):
        for i in self.sc_loaded:
            i.loop(self)

    def setBg(self,r,g,b):
        self.bg_color = (float(r),float(g),float(b),1)
        glClearColor(float(r),float(g),float(b),1)

    def shaderSetup(self,fov,near,far):
        glUniformMatrix4fv(self.program.uniforms["view"], 1, GL_FALSE, numpy.array(glm.mat4(1.0)))
        #glUniformMatrix4fv(self.program.uniforms["projection"], 1, GL_FALSE, numpy.array(glm.mat4(1.0)))
        #glUniformMatrix4fv(self.program.uniforms["view"], 1, GL_FALSE, numpy.array(glm.lookAt((0,0,3),(0,0,-1),(0,1,0))))
        glUniformMatrix4fv(self.program.uniforms["projection"], 1, GL_FALSE, numpy.array(glm.perspective(fov,self.win.x/self.win.y,near,far)))

    def draw(self):
            #for i in self.command:
                #self.command_executer.exec(i,self.to_draw_objects)
            self.command_executer.execute(self.command,self.to_draw_objects)
            self.useScLoop()
            for i in self.to_draw_objects:
                if (i.draw == True):
                    #glUniformMatrix4fv(self.program.uniforms["model"],1,GL_FALSE,i.model)
                    #i.popModel()
                    #i.model = numpy.array(glm.translate(i.model, self.std_move))
                    glUniformMatrix4fv(self.program.uniforms["model"], 1, GL_FALSE, i.model)
                    self.objects[i.name].draw(self.objects[i.name].amount,texBind=i.texture)
                    self.drawGrid(i.model)

    def shaderLoad(self,fld,vertex_shdr,fragment_shdr):
        os.chdir(fld)
        self.shaders = os.listdir()
        for i in self.shaders:
            if (i == vertex_shdr):
                file = open(i,"r")
                data = file.read()
                file.close()
                self.shdr_v = shader_code(data,GL_VERTEX_SHADER)
                break
        for i in self.shaders:
            if (i == fragment_shdr):
                file = open(i,"r")
                data = file.read()
                file.close()
                self.shdr_f = shader_code(data,GL_FRAGMENT_SHADER)
                break
        mod = False
        if (self.shdr_v == None):
            self.error_logs.append(f"Shader {vertex_shdr} does not exist.")
            mod = True
        if (self.shdr_f == None):
            self.error_logs.append(f"Shader {fragment_shdr} does not exist.")
            mod = True
        os.chdir("../")
        if (mod == True):
            return 0
        else:
            try:
                self.program = shader_program([self.shdr_v,self.shdr_f])
                self.program.addUniform("model")
                self.program.addUniform("view")
                self.program.addUniform("projection")
                self.program.addUniform("colorSelector")
                self.error_logs.append("Shader program created.")
            except:
                self.error_logs.append(f"Cannot create program from shaders {self.shdr_f},{self.shdr_v}.")
            if (self.program == None):
                self.error_logs.append(f"Cannot create program from shaders {self.shdr_f},{self.shdr_v}.")

    def start_script_run(self,sc_name):
        if (sc_name.lower() != "none"):
            self.runScript(sc_name)

def mainloop_(core):
    core.runProgram()
    core.shaderSetup(float(cfg_file.collector["fov"]),float(cfg_file.collector["near_plane"]),float(cfg_file.collector["far_plane"]))

    #core.sc_loaded.append(importlib.import_module(f"{core.sc_fld}.jenny_demo").script())
    #core.sc_loaded[-1].setup(core)
    #core.runScript("obamacare")
    core.runMenu()
    while core.runcode == True:
        pygame.time.Clock().tick(int(cfg_file.collector["clock"]))
        core.ctr += 1
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
        for i in pygame.event.get():
            if (i.type == pygame.QUIT):
                print(core.error_logs)
                glDeleteProgram(core.program.program)
                quit()

        core.ioevent()
        core.draw()

        try:
            pygame.display.flip()
        except:
            break

CORE = main_core()
try:
    cfg_file = configuration()
    cfg_file.parse_all("config")
    create_vertex_fld(cfg_file.collector["obj_path"])
    create_vertex_fld(cfg_file.collector["shader_path"])
    create_vertex_fld(cfg_file.collector["scripts"])
    CORE.winCreate(int(cfg_file.collector["win_size_x"]),int(cfg_file.collector["win_size_y"]))
    CORE.load_objects_names(cfg_file.collector["obj_path"])
    CORE.sc_fld = cfg_file.collector["scripts"]
    CORE.shaderLoad(cfg_file.collector["shader_path"],cfg_file.collector["shdr_vertex"],cfg_file.collector["shdr_fragment"])
    CORE.setBg(cfg_file.collector["bg_color_r"],cfg_file.collector["bg_color_g"],cfg_file.collector["bg_color_b"])
    CORE.enableFunctions(bool(int(cfg_file.collector["blend"])),bool(int(cfg_file.collector["depth_test"])))
    CORE.showGridEnable(bool(int(cfg_file.collector["grid"])))
    CORE.start_script_run(cfg_file.collector["start_script"])
except:
    winerror("Cannot load configuration from 'config'")

#print(CORE.error_logs)
mainloop_(CORE)
