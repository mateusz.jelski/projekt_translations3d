from numpy import array
from glm import mat4
from tkinter import *


#____________________________POP_MODEL______________________________________
def POP(core):
        root = Tk()
        type = "pop"

        l1 = Label(root, text="Object Index")
        eInd = Entry(root)
        do = Button(root, text="Execute: Reset Matrix",
                    command=lambda: newInstruction(int(eInd.get()), None, type, core))

        l1.pack()
        eInd.pack()
        do.pack()

        root.mainloop()

    #____________________________POP_MODEL______________________________________
    #____________________________TRANSLATION____________________________________
def TRANSLATE(core):
        root = Tk()
        type = "translate"

        l1 = Label(root, text="Object Index")
        eInd = Entry(root)
        argsInd = Entry(root)
        l2 = Label(root, text="translate: x y z")
        do = Button(root, text="Execute",
                    command=lambda: newInstruction(int(eInd.get()), parse_args(argsInd.get()), type, core))

        l1.pack()
        eInd.pack()
        l2.pack()
        argsInd.pack()
        do.pack()

        root.mainloop()

    #____________________________TRANSLATION____________________________________
    #____________________________SACLE_MENU_____________________________________
def SCALE(core):
        root = Tk()
        type = "scale"

        l1 = Label(root,text="Object Index")
        eInd = Entry(root)
        argsInd = Entry(root)
        l2 = Label(root,text="scale, x y z")
        do = Button(root,text="Execute",command=lambda: newInstruction(int(eInd.get()),parse_args(argsInd.get()),type,core))

        l1.pack()
        eInd.pack()
        l2.pack()
        argsInd.pack()
        do.pack()


        root.mainloop()
    #____________________________SACLE_MENU_____________________________________
    #____________________________ROTATE_MENU____________________________________
def ROTATE(core):
        root = Tk()
        type = "rotate"

        l1 = Label(root, text="Object Index")
        eInd = Entry(root)
        argsInd = Entry(root)
        l2 = Label(root, text="rotate; angle(degrees) x y z")
        do = Button(root, text="Execute",
                        command=lambda: newInstruction(int(eInd.get()), parse_args(argsInd.get()), type, core))

        l1.pack()
        eInd.pack()
        l2.pack()
        argsInd.pack()
        do.pack()

        root.mainloop()
    #____________________________ROTATE_MENU____________________________________

def winerror(label_content):
    root = Tk()
    label = Label(root,text=label_content)
    label.pack()

    root.mainloop()
    quit()

class cmd:

    def __init__(self,obj_index,type,args):
        self.object = obj_index
        self.type = type # scale, translation, rotate, pop
        self.args = args
        self.use = True
        self.validate_self()

    def validate_self(self):
        if (self.type == "scale" or self.type == "translate"):
            if (len(self.args) != 3):
                self.type = "error"
        elif (self.type == "rotate" and len(self.args) != 4):
            self.type = "error"

class pointer:

    def __init__(self):
        self.var = None

class drawing:

    def __init__(self, name, model_matrix):
        self.name = name
        self.model = model_matrix
        self.draw = True
        self.texture = False

    def delete(self):
        del self

    def getModel(self):
        return self.model

    def popModel(self):
        self.model = array(mat4(1.0))

    def setModel(self,mod):
        self.model = mod

def sctl_add(sc_name,core):
    core.sctl.append(sc_name)

def glue(lista,c_ofst=0):
    out = ""
    c = c_ofst
    for i in lista:
        out += f"{c} "
        c+= 1
        out += i
    return out

def seekObjects(objects_list):

    def prev(ptr):
        if (ptr.var != 0):
            ptr.var -= 1
            show_label.config(text=glue(str_list[ptr.var * 15:ptr.var * 15 + 15],c_ofst=ptr.var * 15))
            sites_label.config(text=f"{ptr.var}/{sites}")

    def next(ptr):
        if (ptr.var != sites):
            ptr.var += 1
            show_label.config(text=glue(str_list[ptr.var * 15:ptr.var * 15 + 15],c_ofst=ptr.var * 15))
            sites_label.config(text=f"{ptr.var}/{sites}")

    root = Tk()
    str_list = []
    sites = len(objects_list) // 15
    c_site_ptr = pointer()
    c_site_ptr.var = 0
    for i in range(len(objects_list)):
        str_list.append(objects_list[i].name+"\n")

    show_label = Label(root,text=glue(str_list[c_site_ptr.var*15:c_site_ptr.var*15+15]))
    sites_label = Label(root,text=f"{c_site_ptr.var}/{sites}")
    next_site = Button(root,text="-->",command=lambda:next(c_site_ptr))
    prev_site = Button(root, text="<--",command=lambda: prev(c_site_ptr))
    show_label.pack()
    sites_label.pack()
    prev_site.pack()
    next_site.pack()

    root.mainloop()

def seekCMD(objects_list):

    def prev(ptr):
        if (ptr.var != 0):
            ptr.var -= 1
            show_label.config(text=glue(str_list[ptr.var * 15:ptr.var * 15 + 15],c_ofst=ptr.var * 15))
            sites_label.config(text=f"{ptr.var}/{sites}")

    def next(ptr):
        if (ptr.var != sites):
            ptr.var += 1
            show_label.config(text=glue(str_list[ptr.var * 15:ptr.var * 15 + 15],c_ofst=ptr.var * 15))
            sites_label.config(text=f"{ptr.var}/{sites}")

    root = Tk()
    str_list = []
    sites = len(objects_list) // 15
    c_site_ptr = pointer()
    c_site_ptr.var = 0
    for i in range(len(objects_list)):
        str_list.append(f"type:{objects_list[i].type} object:{objects_list[i].object} args:{objects_list[i].args}\n")

    show_label = Label(root,text=glue(str_list[c_site_ptr.var*15:c_site_ptr.var*15+15]))
    sites_label = Label(root,text=f"{c_site_ptr.var}/{sites}")
    next_site = Button(root,text="-->",command=lambda:next(c_site_ptr))
    prev_site = Button(root, text="<--",command=lambda: prev(c_site_ptr))
    show_label.pack()
    sites_label.pack()
    prev_site.pack()
    next_site.pack()

    root.mainloop()

def arr_template(templ,num):
    f = []
    for i in range(len(templ)):
        b = []
        for l in templ[i]:
            b.append(num)
        f.append(b)
    return f


def command_collector(cmd,drw):
    list_ = []
    for i in range(len(drw)):
        list_.append([])

    for i in cmd:
        list_[i.object].append(i)
    return list_

def operation_center(core_app):

    def update(cObject, boxes_list, command):
        if (cObject.var == len(boxes_list)):
            return 0
        for i in todepack.var:
            i.depackall()
        boxes_list = get_boxes(boxes_list, command, cObject.var)
        pack_boxes(boxes_list, cObject.var)
        todepack.var = boxes_list[cObject.var]
        object_label.config(
            text=f'{cObject.var}/{len(box_ls)-1}, object:{core_app.to_draw_objects[cObject.var].name}')

    class box:

        def __init__(self, win, con, obj, core):
            self.label = Label(win, text=con)
            self.up = Button(win, text="up", command=lambda: self.upf(core))
            self.down = Button(win, text="down", command=lambda: self.downf(core))
            self.delb = Button(win, text="del", command=lambda: self.delcmd(core))
            self.useb = Button(win, text="use", command=lambda: self.setuse())
            self.obj = obj

            if (obj.use == False):
                self.label.config(bg='red')

        def delcmd(self, core_app):
            p = 0
            for i in core_app.command:
                if (i == self.obj):
                    del core_app.command[p]
                    del self
                    break
                p += 1
            update(cObject,arr_template(command_collector(core_app.command,core_app.to_draw_objects),0),command_collector(core_app.command,core_app.to_draw_objects))

        def downf(self, core_app):
            next = False
            prev = 0
            for i in range(len(core_app.command)):
                if (core_app.command[i] == self.obj):
                    next = True
                    prev = i
                elif (next == True and core_app.command[i].object == self.obj.object):
                    core_app.command.insert(i + 1, self.obj)
                    del core_app.command[prev]
                    break
            update(cObject, arr_template(command_collector(core_app.command, core_app.to_draw_objects), 0),
                   command_collector(core_app.command, core_app.to_draw_objects))

        def setuse(self):
            if (self.obj.use == True):
                self.obj.use = False
                self.label.config(bg="red")
            else:
                self.obj.use = True
                self.label.config(bg="white")

        def upf(self, core_app):
            next = False
            prev = 0
            for i in range(len(core_app.command) - 1, -1, -1):
                if (core_app.command[i] == self.obj):
                    next = True
                    prev = i
                elif (next == True and core_app.command[i].object == self.obj.object):
                    del core_app.command[prev]
                    core_app.command.insert(i, self.obj)
                    break
            update(cObject, arr_template(command_collector(core_app.command, core_app.to_draw_objects), 0),
                   command_collector(core_app.command, core_app.to_draw_objects))

        def packall(self, s):
            self.label.grid(row=s, column=0)
            self.up.grid(row=s, column=1)
            self.down.grid(row=s, column=2)
            self.delb.grid(row=s, column=3)
            self.useb.grid(row=s, column=4)

        def depackall(self):
            self.label.grid_forget()
            self.up.grid_forget()
            self.down.grid_forget()
            self.delb.grid_forget()
            self.useb.grid_forget()

    def plus_add_object(core):
        root = Tk()

        trans = Button(root,text="translate",command=lambda: TRANSLATE(core))
        rotate = Button(root,text="rotate",command=lambda: ROTATE(core))
        scale = Button(root,text="scale",command=lambda: SCALE(core))

        trans.pack()
        rotate.pack()
        scale.pack()

        root.mainloop()

    def get_boxes(empty_list,command_list,cObject):
        #if (len(commands) != 0):
            for l in range(len(command_list[cObject])):
                empty_list[cObject][l] = box(root,
                                         f"{l} object:{command_list[cObject][l].object}, type:{command_list[cObject][l].type}, object:{command_list[cObject][l].object}, args:{command_list[cObject][l].args}",
                                         command_list[cObject][l], core_app)
                #empty_list[cObject][l].packall(l)
            return empty_list

    def pack_boxes(box_list,cObject):
        lenght=1
        #if (prevObject >= 0):
            #for i in box_list[prevObject]:
                #i.depackall()
        for l in box_list[cObject]:
            l.packall(lenght)
            lenght+=1

    def nextObject(cObject,boxes_list,command):
        if (cObject.var == len(boxes_list)-1):
            return 0
        for i in todepack.var:
            i.depackall()
        boxes_list = get_boxes(boxes_list,command,cObject.var+1)
        pack_boxes(boxes_list,cObject.var + 1)
        cObject.var += 1
        todepack.var = boxes_list[cObject.var]
        object_label.config(text=f'{cObject.var}/{len(box_ls) - 1}, object:{core_app.to_draw_objects[cObject.var].name}')

    def prevObject(cObject,boxes_list,command):
        if (cObject.var == 0):
            return 0
        for i in todepack.var:
            i.depackall()
        boxes_list = get_boxes(boxes_list,command,cObject.var-1)
        pack_boxes(boxes_list,cObject.var - 1)
        cObject.var -= 1
        todepack.var = boxes_list[cObject.var]
        object_label.config(text=f'{cObject.var}/{len(box_ls)-1}, object:{core_app.to_draw_objects[cObject.var].name}')

    commands = command_collector(core_app.command,core_app.to_draw_objects)
    box_ls = arr_template(commands,0)
    cObject = pointer()
    cObject.var = 0
    todepack = pointer()
    todepack.var = []

    root = Tk()
    try:
        object_label = Label(root,text=f'{cObject.var}/{len(box_ls)-1}, object:{core_app.to_draw_objects[cObject.var].name}')
    except:
        object_label = Label(root,text=f'{cObject.var}/{len(box_ls) - 1}, object: NONE')
        object_label.grid(row=0, column=0)
        return
    nextButton = Button(root,text="->",command=lambda: nextObject(cObject,arr_template(command_collector(core_app.command,core_app.to_draw_objects),0),command_collector(core_app.command,core_app.to_draw_objects)))
    prevButton = Button(root, text="<-",command=lambda: prevObject(cObject,arr_template(command_collector(core_app.command,core_app.to_draw_objects),0),command_collector(core_app.command,core_app.to_draw_objects)))
    addButton = Button(root,text="+",command=lambda: plus_add_object(core_app))
    object_label.grid(row=0,column=0)
    nextButton.grid(row=0,column=1)
    prevButton.grid(row=0,column=2)
    addButton.grid(row=0,column=3)
    box_ls = get_boxes(box_ls,commands,cObject.var)
    pack_boxes(box_ls,cObject.var)
    todepack.var = box_ls[0]


    root.mainloop()

def parse_args(string):
    tmp = string.split(" ")
    output = []
    for i in range(len(tmp)):
        try:
            output.append(float(tmp[i]))
        except:
            pass
    return output

def isKey(dict,key):
    try:
        a = dict[key]
        return True
    except:
        return False

def newInstruction(object_index,args,type,core_app):
    com = cmd(object_index,type,args)
    try:
        a = core_app.to_draw_objects[object_index]
        core_app.command.append(com)
    except:
        core_app.error_logs.append("Cannot find valid object")

def addNewDrawing(mat,name,core_app):
    if (isKey(core_app.objects,name) == True):
        core_app.to_draw_objects.append(drawing(name,mat))
        core_app.error_logs.append(f"Added object: {name}")
    else:
        core_app.error_logs.append(f"Bad object name: {name}")