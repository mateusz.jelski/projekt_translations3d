import PIL.Image
import cls_lib
from math import pi,sin
from glm import rotate,translate
from OpenGL.GL import GL_RGBA,GL_ARRAY_BUFFER,GL_STATIC_DRAW,GL_FLOAT,GL_TRUE,GL_ELEMENT_ARRAY_BUFFER,GL_TRIANGLES
import numpy

class script:

    def __init__(self):
        self.id = None
        self.angle = 0

    def setup(self,core_app):
        image = PIL.Image.open(r"sc\pyramid\default.png")
        image.convert("RGBA")
        tex = core_app.createTexture(image.tobytes(),GL_RGBA,385,385)
        del image

        vertex = numpy.array([
            -0.5, -0.5, 0.0,  0,0,
            0.5, -0.5, 0.0,   1,0,
            0.5, 0.5, 0.0,    1,1,
            -0.5, 0.5, 0.0,   0,1,
            0.0, 0.0, 0.8,    0.5,0.5
        ],dtype="float32")
        ind = numpy.array([
            0, 1, 2, 1, 2, 3,
            0, 1, 4, 3, 0, 4, 1, 2, 4, 3, 2, 4
        ],dtype="uint")
        vao = cls_lib.vBuffer()
        vbo = cls_lib.buffer(GL_ARRAY_BUFFER, vertex, GL_STATIC_DRAW)
        ebo = cls_lib.buffer(GL_ELEMENT_ARRAY_BUFFER, ind, GL_STATIC_DRAW)
        vao.newAttrib(3, GL_FLOAT, GL_TRUE, 20)
        vao.newAttrib(2, GL_FLOAT, GL_TRUE, 20, 12)

        core_app.customModelApply(GL_TRIANGLES,vao,ebo,vbo,"prism_tex",ind.size)

        id = core_app.objectCreate("prism_tex")
        core_app.bindTextureTo(tex,id)
        self.id = id

    def loop(self,core_app):
        mod = core_app.getObjectByIndex(self.id)
        self.angle += 0.06
        mod.model = numpy.array(translate(cls_lib.mat(),(0,sin(self.angle)/10,-3)))
        mod.model = numpy.array(rotate(mod.model,pi/2,(-1,0,0)))
        mod.model = numpy.array(rotate(mod.model,self.angle, (0, 0, 1)))
